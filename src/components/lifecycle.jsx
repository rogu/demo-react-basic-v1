import * as React from 'react';
import PropTypes from 'prop-types';

class LifeCycleComponent extends React.Component {

    static propTypes = {
        title: PropTypes.string.isRequired
    }

    constructor(props) {
        super(props);
        console.log('constructor', props);
    }

    componentWillMount() {
        console.log('componentWillMount');
    }

    componentDidMount() {
        console.log('componentDidMount');
    }

    componentWillReceiveProps(props) {
        console.log('componentWillReceiveProps', props);
    }

    componentWillUpdate(props) {
        console.log('componentWillUpdate', props);
    }

    componentDidUpdate(props) {
        console.log('componentDidUpdate', props);
    }

    componentWillUnmount(props) {
        console.log('componentWillUnmount', props);
    }

    shouldComponentUpdate(props) {
        console.log('shouldComponentUpdate', props);
        return true;
    }

    render() {
        console.log('render');
        return (
            <div>
                <h4>{this.props.title} - zobacz logi</h4>
                <hr/>
                <h5>Montowanie (mount) - poniższe metody wykonywane są podczas tworzenia komponentu wg poniższej kolejności</h5>
                <ul>
                    <li>constructor - można zainicjować stan</li>
                    <li>componentWillMount - wykonuje się tuż przed pierwszym wywołaniem metody render</li>
                    <li>render - wyświetlenie stanu</li>
                    <li>componentDidMount - wykonuje się tuż po pierwszym wywołaniu metody render</li>
                </ul>
                <h5>Odświeżanie (update) - poniższe metody wykonują się po zmianie stanu lub propsów</h5>
                <ul>
                    <li>componentWillReceiveProps - wykonuje się w momencie aktualizacji propsów</li>
                    <li>shouldComponentUpdate - pozwala zatrzymać re-renderowanie</li>
                    <li>componentWillUpdate - wykonuje się tuż przed re-renderowaniem</li>
                    <li>render - kolejne re-renderowanie po aktualizacji stanu</li>
                    <li>componentDidUpdate - wykonuje się po re-renderowaniu</li>
                </ul>
                <h5>Demontowanie (unmount)</h5>
                <ul>
                    <li>componentWillUnmount - wykonuje się tuż przed usunięciem komponentu z drzewa DOM</li>
                </ul>
            </div>
        )
    }
}
export default LifeCycleComponent;

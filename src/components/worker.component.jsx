import React, { Component } from "react";
import PropTypes from "prop-types";

class WorkerComponent extends Component {

  static propTypes = {
    data: PropTypes.object.isRequired,
    selected: PropTypes.bool.isRequired,
    phoneChange: PropTypes.func.isRequired,
    callEvent: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      contact: props.data
    };
  }

  updatePhone(evt) {
    this.setState(
      { contact: { ...this.state.contact, phone: evt.target.value } },
      () => this.props.phoneChange({ ...this.state.contact })
    );
  }

  render(param) {
    return (
      <div className="card card-body mb-2 bg-light">
        <h5>{this.props.data.name}</h5>
        <div className="form-row">
          <input
            type="text"
            className="form-control col"
            onChange={this.updatePhone.bind(this)}
            value={this.state.contact.phone} />
          <button
            className={`btn ${this.props.selected && 'btn-secondary'} col`}
            onClick={() => { this.props.callEvent(this.state.contact) }}>
            show phone
          </button>
        </div>
      </div>
    );
  }
}

export default WorkerComponent;

import * as React from 'react';
import PropTypes from 'prop-types';

class GalleryComponent extends React.Component {

    myStyle = {
        cursor: 'pointer'
    }

    static propTypes = {
        data: PropTypes.array.isRequired,
        onChange: PropTypes.func.isRequired
    }

    render() {
        return <div>
            {this.props.data.map((image, idx) => {
                return <img alt='gal' className="img-thumbnail m-2" style={this.myStyle} key={idx} src={image} onClick={this.props.onChange} width="20%"/>
            })}
        </div>
    }
}
export default GalleryComponent;

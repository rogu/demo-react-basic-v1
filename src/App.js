import React, { Component } from "react";
import "./App.css";
import { createStore } from 'redux'
import WorkerComponent from "./components/worker.component";
import GalleryComponent from "./components/gallery.component";
import LifeCycleComponent from "./components/lifecycle";
import "reusable-webc/dist/webc/footer";

function testReducer(initData = [], action) {
  switch (action.type) {
    case 'PUSH':
      return [...initData, action.payload];
    default:
      return initData
  }
}

class App extends Component {
  state = {
    myName: "Robert",
    access: false,
    selectedItem: null,
    selectedWorker: null,
    selectedImage: null,
    items: [
      { name: "tomato" },
      { name: "potatoes" },
      { name: "apple" }
    ],
    contacts: [
      { name: "Joe", phone: 1234234 },
      { name: "Mike", phone: 81234234 }
    ],
    images: [
      'https://api.debugger.pl/assets/pepper.jpg',
      'https://api.debugger.pl/assets/pumpkin.jpg',
      'https://api.debugger.pl/assets/cabbage.jpg'
    ],
    fromStore: []
  };

  constructor() {
    super();
    this.store = createStore(testReducer);
    this.store.subscribe((value) => {
      this.setState({ fromStore: this.store.getState() })
    })
  }

  updateName(evt) {
    this.setState({ myName: evt.target.value });
  }

  pushToStore() {
    this.store.dispatch({ type: "PUSH", payload: new Date().toLocaleTimeString() });
  }

  checkedChandler({ target: { checked } }) {
    this.setState({ access: !checked });
  }

  selectItem(selectedItem) {
    this.setState({ selectedItem });
  }

  selectImage({ target: { src } }) {
    this.setState({ selectedImage: src });
  }

  updatePhone(contact) {
    this.setState({
      contacts: this.state.contacts.map(item => {
        if (item.name === contact.name)
          return { ...item, phone: contact.phone };
        else
          return item;
      })
    });
  }

  callToWorker(selectedWorker) {
    this.setState({ selectedWorker });
  }

  render() {
    return (
      <div className="App container-fluid">

        <div className="row mt-3">

          <div className="col-4">

            <article className="card text-white bg-dark">
              <h4 className="card-header">state</h4>
              <div className="card-body">
                <pre className="text-white">{JSON.stringify(this.state, null, 1)}</pre>
              </div>
            </article>

          </div>

          <div className="col-8">

            <div className="row">
              <div className="col-6">
                <article className="card">
                  <h4 className="card-header">display, change, condition</h4>
                  <div className="card-body">
                    {this.state.myName.toUpperCase()}
                    <br />
                    <input className="form-control" value={this.state.myName} disabled={!this.state.access} onChange={this.updateName.bind(this)} />
                    <br />
                    <label>
                      <input checked={!this.state.access} onChange={this.checkedChandler.bind(this)} type="checkbox" />
                      {this.state.access ? 'editable' : ' not editable'}
                    </label>
                  </div>
                </article>
              </div>

              <div className="col-6">
                <article className="card">
                  <h4 className="card-header">loop & events</h4>
                  <div className="card-body">
                    {this.state.selectedItem}
                    <ul>
                      {this.state.items.map((item, index) => {
                        return <li key={index} className="item">
                          {item.name}
                          <button className='btn btn-secondary m-1' onClick={() => this.selectItem(item.name)}>select</button>
                        </li>;
                      })}
                    </ul>
                  </div>
                </article>
              </div>
            </div>

            <div className="row mt-3">

              <div className="col-12">
                <article className="card">
                  <h4 className="card-header">component</h4>
                  <div className="card-body">

                    <div className="row">
                      <div className="col-6">
                        <article className="card">
                          <h4 className="card-header">
                            Workers list
                          </h4>
                          <div className="card-body">
                            <div className="badge badge-info">{JSON.stringify(this.state.selectedWorker)}</div>
                            {this.state.contacts.map((contact, idx) => {
                              return <WorkerComponent
                                key={idx}
                                selected={this.state.selectedWorker === contact}
                                data={contact}
                                callEvent={this.callToWorker.bind(this)}
                                phoneChange={this.updatePhone.bind(this)} />;
                            })}
                          </div>
                        </article>
                      </div>
                      <div className="col-6">
                        <article className="card">
                          <h4 className="card-header">
                            Gallery
                          </h4>
                          <div className="card-body text-center">
                            <GalleryComponent onChange={this.selectImage.bind(this)} data={this.state.images} />
                            <img src={this.state.selectedImage} className="w-50" alt='react' />
                          </div>
                        </article>
                      </div>
                    </div>

                  </div>
                </article>
              </div>

            </div>

            <div className="row mt-3">

              <div className="col-6">
                <article className="card">
                  <h4 className="card-header">store</h4>
                  <div className="card-body">
                    <button className="btn btn-secondary" onClick={this.pushToStore.bind(this)}>push to store</button>
                    <div>{this.state.fromStore.join('; ')}</div>
                  </div>
                </article>
              </div>

              <div className="col-6"></div>

            </div>

          </div>

        </div>

        <article className="card mt-3 mb-3 bg-info text-white">
          <h4 className="card-header">Component lifecycle</h4>
          <div className="card-body">
            <LifeCycleComponent title={'Cykl życia komponentu'} />
          </div>
        </article>

        <course-footer courses></course-footer>
      </div>

    );
  }
}

export default App;
